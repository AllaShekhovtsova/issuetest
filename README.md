# Siden Test Automation framework
The project is designed to provide  tools to automate Siden API and Front-end test cases

## Installation and configuration
## Pull the project to your local workspace
```shell
git clone git@gitlab.com:sidenio/qe/test-automation.git  
```
As the result you can see the ``test-atomation`` folder at your current workplace.

## gRPC code generation
to generate gRPC Java classes we need to build the project locally using the command
```shell
mvn clean package -DskipTests
```
if the building is successful we can see the following folder
- `./target/generated-sources/protobuf/grpc-java`
- `./target/generated-sources/protobuf/java`

the classes should be available in `.classpath`, for example, for the IntelliJ project the project structure is described
in `test-automation.iml` file. The generated gRPC folders should be in the file, like
```xml
<component name="NewModuleRootManager" LANGUAGE_LEVEL="JDK_1_8">
<output url="file://$MODULE_DIR$/target/classes" />
<output-test url="file://$MODULE_DIR$/target/test-classes" />
<content url="file://$MODULE_DIR$">
<sourceFolder url="file://$MODULE_DIR$/src/main/java" isTestSource="false" />
<sourceFolder url="file://$MODULE_DIR$/src/test/java" isTestSource="true" />
<sourceFolder url="file://$MODULE_DIR$/src/test/resources" type="java-test-resource" />
<sourceFolder url="file://$MODULE_DIR$/target/generated-sources/protobuf/grpc-java" isTestSource="false" generated="true" />
<sourceFolder url="file://$MODULE_DIR$/target/generated-sources/protobuf/java" isTestSource="false" generated="true" />
<excludeFolder url="file://$MODULE_DIR$/target" />
</content>
</component>
```

## Configure your local environment security settings
There are two ways to configure your local workspace to be able to be successfully authorized on backend service.

First, you can use environment variables to configure your local workspace for the specific test environment. 
You can call the following ``bash`` commands or put them to your .bashrc file
```shell
  export CLIENT_ID = <environment_client_id>
  export CLIENT_SECRET = <environment_client_secret> 
  export PASSWORD = "<your_password>"
  export USERNAME =  "<your_username>"
```

the other way, you can put your temporar OAuth2 token to a proper environment configuration file, for example
``src/test/resource/properties/dev.properties``
and initialize ``authToken=``
for example, 
```properties
authToken=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiU2lkZW5BdWRpZW5jZSJdLCJleHAiOjE2MjI2NDI0NDAsImh0dHBzOi8vc2
```
that would be enough for successful authorization, but don't commit this token to a repository.

## Running the test cases
You can easily ron all the tests against the specific test environment just executing a command
```shell
mvn clean verify
```

## Reporting
The reports can be generated using the following command
```shell
allure serve target/surfire-reports
```